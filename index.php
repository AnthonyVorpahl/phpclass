<!DOCTYPE html>
    <html lang="en">
<head>
    <meta charset="utf-8">
    <title>Anthony's Homepage</title>
    <link rel="stylesheet" type="text/css" href="css/base.css" />
</head>
<body>
<header><h1>Anthony's Thingger</h1></header>
<nav>
    <ul>
        <li><a href="/">Homepage</a></li>
        <li><a href="#">Loop</a></li>
        <li><a href="#">Count Down</a></li>
    </ul>
</nav>
<main>
    <img src="images/tony.jpg" alt="Picture of Tony" />
    <p>My name is Tony. I run an elite Beast machine. I am a father. I also do spray paint art.</p>
</main>
    <footer>&copy; 2019 AnthonyVorpahl.com</footer>

</body>
</html>
